
CREATE TABLE Usuario (
	login VARCHAR(20) NOT NULL,
	nome_completo VARCHAR(50) NOT NULL,
	cidade_natal VARCHAR(20),
	data_de_nascimento DATE,
	PRIMARY KEY(login)
);

CREATE TABLE Bloqueia (
	login1 VARCHAR(20) NOT NULL,
	login2 VARCHAR(20) NOT NULL,
	razao VARCHAR(200),
	PRIMARY KEY(login1, login2),
	FOREIGN KEY(login1)
		REFERENCES Usuario(login)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY(login2)
		REFERENCES Usuario(login)
			ON DELETE CASCADE
			ON UPDATE CASCADE
);

CREATE TABLE Registra (
	login1 VARCHAR(20) NOT NULL,
	login2 VARCHAR(20) NOT NULL,
	PRIMARY KEY(login1, login2),
	FOREIGN KEY(login1)
		REFERENCES Usuario(login)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY(login2)
		REFERENCES Usuario(login)
			ON DELETE CASCADE
			ON UPDATE CASCADE
);


CREATE TABLE Cineasta (
	id INTEGER NOT NULL,
	nome VARCHAR(50) NOT NULL,
	telefone INTEGER NOT NULL,
	endereco VARCHAR(200) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Diretor (
	id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Cineasta(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Ator (
	id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Cineasta(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Filme (
	id VARCHAR(20) NOT NULL,
	nome VARCHAR(100),
	data_de_lancamento DATE,
	id_diretor INTEGER,
	salario_diretor INTEGER,
	id_categoria INTEGER,
	PRIMARY KEY(id),
	FOREIGN KEY(id_diretor)
		REFERENCES Diretor(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_categoria)
		REFERENCES Categoria(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Categoria (
	id INTEGER NOT NULL,
	descricao VARCHAR(20) NOT NULL,
	superCategoria INTEGER, 
	PRIMARY KEY(id),
	FOREIGN KEY(superCategoria)
		REFERENCES Categoria(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Atua (
	id_filme VARCHAR(20) NOT NULL,
	id_ator INTEGER NOT NULL,
	salario INTEGER,
	PRIMARY KEY(id_filme, id_ator),
	FOREIGN KEY(id_filme)
		REFERENCES Filme(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_ator)
		REFERENCES Ator(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Curte_filme (
	login VARCHAR(20) NOT NULL,
	id_filme VARCHAR(20) NOT NULL,
	nota INTEGER NOT NULL,
	PRIMARY KEY(login, id_filme),
	FOREIGN KEY(login)
		REFERENCES Usuario(login)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_filme)
		REFERENCES Filme(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Artista_musical (
	id VARCHAR(50) NOT NULL,
	nome_artistico VARCHAR(50),
	pais VARCHAR(20),
	genero VARCHAR(20),
	PRIMARY KEY(id)
);

CREATE TABLE Musico (
	id VARCHAR(50) NOT NULL,
	nome_real VARCHAR(50),
	data_de_nascimento DATE,
	estilo_musical VARCHAR(50),
	PRIMARY KEY(id)
);

CREATE TABLE Curte_artista_musical (
	login VARCHAR(50) NOT NULL,
	id_artista VARCHAR(50) NOT NULL,
	nota INTEGER NOT NULL,
	PRIMARY KEY(login, id_artista),
	FOREIGN KEY(login)
		REFERENCES Usuario(login)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY(id_artista)
		REFERENCES Artista_musical(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
);

CREATE TABLE Cantor (
	id VARCHAR(20) NOT NULL,
	id_musico VARCHAR(20) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id_musico)
		REFERENCES Musico(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Dupla (
	id VARCHAR(20) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Artista_musical(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE DuplaContem (
	id_dupla VARCHAR(20) NOT NULL,
	id_musico1 VARCHAR(20) NOT NULL,
	id_musico2 VARCHAR(20) NOT NULL,
	PRIMARY KEY(id_dupla, id_musico1, id_musico2),
	FOREIGN KEY(id_dupla)
		REFERENCES Dupla(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_musico1)
		REFERENCES Musico(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_musico1)
		REFERENCES Musico(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);


CREATE TABLE Grupo (
	id VARCHAR(20) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Artista_musical(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE GrupoContem (
	id_grupo VARCHAR(20) NOT NULL,
	id_musico VARCHAR(20) NOT NULL,
	PRIMARY KEY(id_grupo, id_musico),
	FOREIGN KEY(id_grupo)
		REFERENCES Grupo(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(id_musico)
		REFERENCES Musico(id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);





INSERT INTO Usuario VALUES ('HomemDeFerro','Anthony Edward Stark','Nova York');
INSERT INTO Usuario VALUES ('DeusDoTrovao','Thor Odinson','Asgard');
INSERT INTO Usuario VALUES ('ViuvaNegra','Natalia Alianova Romanova','Volgogrado');
INSERT INTO Usuario VALUES ('DoutorEstranho','Stephen Vincent Strange','Filadelfia');
INSERT INTO Usuario VALUES ('DeadPool','Wade Winston Wilson','Toronto');
INSERT INTO Usuario VALUES ('HomemAranha','Peter Benjamin Parker','Nova York');
INSERT INTO Usuario VALUES ('Thanos','Thanos de Tita','Tita');

INSERT INTO Registra VALUES ('HomemDeFerro','DoutorEstranho');
INSERT INTO Registra VALUES ('HomemDeFerro','ViuvaNegra');
INSERT INTO Registra VALUES ('HomemDeFerro','DeadPool');
INSERT INTO Registra VALUES ('HomemAranha','HomemDeFerro');
INSERT INTO Registra VALUES ('HomemAranha’,'DeadPool');
INSERT INTO Registra VALUES ('DoutorEstranho','HomemDeFerro');
INSERT INTO Registra VALUES ('DoutorEstranho','DeusDoTrovao');
INSERT INTO Registra VALUES ('DeadPool','HomemDeFerro');
INSERT INTO Registra VALUES ('DeadPool','ViuvaNegra');
INSERT INTO Registra VALUES ('Thanos','HomemDeFerro');
INSERT INTO Registra VALUES ('DeusDoTrovao','ViuvaNegra');

INSERT INTO Bloqueia VALUES ('HomemDeFerro','Thanos','Matou metade do universo');
INSERT INTO Bloqueia VALUES ('DeusDoTrovao','Thanos','Matou metade do universo');
INSERT INTO Bloqueia VALUES ('ViuvaNegra','Thanos','Matou metade do universo');
INSERT INTO Bloqueia VALUES ('DoutorEstranho','Thanos','Matou metade do universo');
INSERT INTO Bloqueia VALUES ('DeadPool','Thanos','Matou metade do universo');
INSERT INTO Bloqueia VALUES ('HomemAranha','Thanos','Nao estou me sentindo muito bem Sr Stark');

INSERT INTO Cineasta VALUES ('001','David Frankel','001001','Estados Unidos');
INSERT INTO Cineasta VALUES ('002','Phylida Lloyd','002002','Inglaterra');
INSERT INTO Cineasta VALUES ('003','Christopher Nolan','003003','Estados Unidos');
INSERT INTO Cineasta VALUES ('007','John Glen','007007','Inglaterra');
INSERT INTO Cineasta VALUES ('004','Ryan Rodney Reynolds','004004','Toronto');
INSERT INTO Cineasta VALUES ('009','Anne Jacqueline Hathaway','009009','Nova Iorque, Nova Iorque, Estados Unidos');
INSERT INTO Cineasta VALUES ('006','Robert John Downey Jr.','006006','Manhattan,Nova Iorque,Estados Unidos');
INSERT INTO Cineasta VALUES ('008','Josh James Brolin','008008','Los Angeles, California,Estados Unidos');
INSERT INTO Cineasta VALUES ('010','Mary Louise Streep','00100010','Estados Unidos');
INSERT INTO Cineasta VALUES ('011','Daniel Wroughton Craig','00110011','Chester, Cheshire, Reino Unido');
INSERT INTO Cineasta VALUES ('012','Martin Campbell','00120012','Nova Zelândia');
INSERT INTO Cineasta VALUES ('013','Joe Russo','00130013','Estados Unidos');


INSERT INTO Diretor VALUES ('001');
INSERT INTO Diretor VALUES ('002');
INSERT INTO Diretor VALUES ('003');
INSERT INTO Diretor VALUES ('007');
INSERT INTO Diretor VALUES ('012');
INSERT INTO Diretor VALUES ('013');

INSERT INTO Ator VALUES ('9');
INSERT INTO Ator VALUES ('10');
INSERT INTO Ator VALUES ('11');
INSERT INTO Ator VALUES ('8');
INSERT INTO Ator VALUES ('4');

INSERT INTO Atua VALUES ('2','10','70000');
INSERT INTO Atua VALUES ('3','9','20000');
INSERT INTO Atua VALUES ('7','11','100000');
INSERT INTO Atua VALUES ('1','4','1');
INSERT INTO Atua VALUES ('4','8','100000');
INSERT INTO Atua VALUES ('5','8','1000000');

INSERT INTO Categoria VALUES ('0','Autobiografia');
INSERT INTO Categoria VALUES ('1','Comedia');
INSERT INTO Categoria VALUES ('2','Super Heroi');
INSERT INTO Categoria VALUES ('3','Açao');
INSERT INTO Categoria VALUES ('4','Comedia','0');
INSERT INTO Categoria VALUES ('5','Romance','1');
INSERT INTO Categoria VALUES ('6','Açao','2');
INSERT INTO Categoria VALUES ('7','Aventura','3');


INSERT INTO Filme VALUES ('001','Lanterna Verde','19/08/2011','012','10','6');
INSERT INTO Filme VALUES ('002','Mamma Mia!','12/09/2008','002','500000','5');
INSERT INTO Filme VALUES ('003','Batman','18/07/2008','003','1000000000','6');
INSERT INTO Filme VALUES ('007','007','13/06/1989','007','700000000','7');
INSERT INTO Filme VALUES ('004','Guerra Infinita','26/04/2018','13','100000000','6');
INSERT INTO Filme VALUES ('005','Ultimato','24/04/2019','13','100000000','6');

INSERT INTO Curte_filme VALUES ('HomemDeFerro','2','10');
INSERT INTO Curte_filme VALUES ('DeusDoTrovao','2','10');
INSERT INTO Curte_filme VALUES ('DeadPool','2','10');
INSERT INTO Curte_filme VALUES ('HomemAranha','2','10');
INSERT INTO Curte_filme VALUES ('Thanos','2','10');
INSERT INTO Curte_filme VALUES ('HomemDeFerro','3','3');
INSERT INTO Curte_filme VALUES ('DeusDoTrovao','3','3');
INSERT INTO Curte_filme VALUES ('DeadPool','3','2');
INSERT INTO Curte_filme VALUES ('HomemAranha','3','1');
INSERT INTO Curte_filme VALUES ('Thanos','3','10');
INSERT INTO Curte_filme VALUES ('ViuvaNegra','7','10');
INSERT INTO Curte_filme VALUES ('ViuvaNegra','2','10');
INSERT INTO Curte_filme VALUES ('ViuvaNegra','3','1');
INSERT INTO Curte_filme VALUES ('ViuvaNegra','3','1');
INSERT INTO Curte_filme VALUES ('ViuvaNegra','1','0');
INSERT INTO Curte_filme VALUES ('HomemDeFerro','1','0');
INSERT INTO Curte_filme VALUES ('DeusDoTrovao','1','0');
INSERT INTO Curte_filme VALUES ('DeadPool','1','0');
INSERT INTO Curte_filme VALUES ('HomemAranha','1','0');
INSERT INTO Curte_filme VALUES ('Thanos','1','9');
INSERT INTO Curte_filme VALUES ('Thanos','4','10');

INSERT INTO Musico VALUES ('1','Ariana Grande-Butera','28/03/1993','Pop');
INSERT INTO Musico VALUES ('2','Brian Harold May','19/07/1947','Rock');
INSERT INTO Musico VALUES ('3','Farrokh Bulsara','05/09/1946','Rock');
INSERT INTO Musico VALUES ('4','John Richard Deacon','19/08/1951','Rock');
INSERT INTO Musico VALUES ('5','Roger Meddows Taylor','26/07/1949','Rock');
INSERT INTO Musico VALUES ('6','José Lima Sobrinho ','05/05/1954','Sertanejo');
INSERT INTO Musico VALUES ('7','Durval de Lima','30/09/1957','Sertanejo');
INSERT INTO Musico VALUES ('8','Madonna Louise Ciccone','16/08/1958','Pop');
INSERT INTO Musico VALUES ('9','Stefani Joanne Angelina Germanotta','28/03/1986','Pop');
INSERT INTO Musico VALUES ('10','Agnetha Åse Fältskog','05/04/1950','Disco');
INSERT INTO Musico VALUES ('11','Goran Bror Benny Anderson','16/12/1946','Disco');
INSERT INTO Musico VALUES ('12','Björn Kristian Ulvaeus','16/04/1945','Disco');
INSERT INTO Musico VALUES ('13','Anni-Frid Synni Lyngstad Reuss','15/11/1945','Disco');
INSERT INTO Musico VALUES ('14','Angus McKinnon Youg','31/03/1955','Rock');
INSERT INTO Musico VALUES ('15','Christopher Rees','30/10/1946','Rock');
INSERT INTO Musico VALUES ('16','Stephen Crawford Young Jr.','11/12/1956','Rock');
INSERT INTO Musico VALUES ('17','Sandy Leah Lima','28/01/1983','Pop');
INSERT INTO Musico VALUES ('18','Durval de Lima Junior','11/04/1984','Pop');
INSERT INTO Musico VALUES ('19','João Salvador Perez','02/03/1917','Sertanejo');
INSERT INTO Musico VALUES ('20','José Perez','19/11/1920','Sertanejo');

INSERT INTO Artista_musical VALUES ('1','Ariana Grande','Estados Unidos','Pop');
INSERT INTO Artista_musical VALUES ('2','Queen','Inglaterra','Rock');
INSERT INTO Artista_musical VALUES ('3','Chitaozinho e Xororo','Brasil','Sertanejo');
INSERT INTO Artista_musical VALUES ('4','Madonna','Estados Unidos','Pop');
INSERT INTO Artista_musical VALUES ('5','Lady Gaga','Estados Unidos','Pop');
INSERT INTO Artista_musical VALUES ('6','ABBA','Suécia','Disco');
INSERT INTO Artista_musical VALUES ('7','AC/DC','Austrália','Rock');
INSERT INTO Artista_musical VALUES ('8','Sandy&Junior','Brasil','Pop');
INSERT INTO Artista_musical VALUES ('9','Tonico&Tinoco','Brasil','Sertanejo');

INSERT INTO Cantor VALUES ('1','1');
INSERT INTO Cantor VALUES ('8','8');
INSERT INTO Cantor VALUES ('9','9');

INSERT INTO Dupla VALUES ('3');
INSERT INTO Dupla VALUES ('8');
INSERT INTO Dupla VALUES ('9');

INSERT INTO Grupo VALUES ('2');
INSERT INTO Grupo VALUES ('6');
INSERT INTO Grupo VALUES ('7');

INSERT INTO DuplaContem VALUES ('3','6','7');
INSERT INTO DuplaContem VALUES ('8','17','18');
INSERT INTO DuplaContem VALUES ('9','19','20');


INSERT INTO GrupoContem VALUES ('2','2');
INSERT INTO GrupoContem VALUES ('2','3');
INSERT INTO GrupoContem VALUES ('2','4');
INSERT INTO GrupoContem VALUES ('2','5');
INSERT INTO GrupoContem VALUES ('6','10');
INSERT INTO GrupoContem VALUES ('6','11');
INSERT INTO GrupoContem VALUES ('6','12');
INSERT INTO GrupoContem VALUES ('6','13');
INSERT INTO GrupoContem VALUES ('7','14');
INSERT INTO GrupoContem VALUES ('7','15');
INSERT INTO GrupoContem VALUES ('7','16');

INSERT INTO Curte_artista_musical VALUES ('HomemDeFerro','2','10');
INSERT INTO Curte_artista_musical VALUES ('DeadPool','5','10');
INSERT INTO Curte_artista_musical VALUES ('ViuvaNegra','4','8');
INSERT INTO Curte_artista_musical VALUES ('DeusDoTrovao','2','10');
INSERT INTO Curte_artista_musical VALUES ('Thanos','3','10');
INSERT INTO Curte_artista_musical VALUES ('HomemAranha','4','10');
INSERT INTO Curte_artista_musical VALUES ('DoutorEstranho','2','9');
INSERT INTO Curte_artista_musical VALUES ('HomemDeFerro','7','10');





















