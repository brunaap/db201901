import psycopg2

import requests
import urllib.request
from bs4 import BeautifulSoup       # importe as funções BeautifulSoup para analisar os dados retornados do site
from lxml import *                  # para editar os dados xml
import random
import networkx as nx
import matplotlib.pyplot as plt

class DataAccess:
    def __init__(self, dbname="1901Banqueta_de_Cubos", user="1901Banqueta_de_Cubos", password="899675", host="200.134.10.32"): 
        self.dbname     = dbname
        self.user       = user
        self.password   = password
        self.host       = host

    def Connect(self):
        try:
            self.conexao = psycopg2.connect(dbname=self.dbname, user=self.user, password=self.password, host=self.host)
            self.cursor = self.conexao.cursor()
            print("Conectado ao banco de dados: " + self.dbname + " :)")
        except:
            print("Não foi possível conectar ao banco de dados: " + self.dbname + " :(")
        
    def Execute(self, comando):
        fetchresult = []
        try:
            self.cursor.execute(comando)
            for tabela in self.cursor.fetchall():
                fetchresult.append(tabela)
        except Exception as e:
            print(e)
        self.conexao.commit()
        return fetchresult
    
    def getUsers(self):
        return self.Execute('Select * from usuario')
    
    def getFriendship(self):
        return self.Execute('Select * from registra')
    
    def getMusicalLikes(self):
        return self.Execute('Select * from curte_artista_musical')
    
    def getArtists(self):
        return self.Execute('Select id from artista_musical')
    
    def getMovies(self):
        return self.Execute('Select * from curte_filme')

class DBScan:
    corepoints = []
    clusters = []
    outlier = []

    def __init__(self, mindist, minconn):
        self.mindist = mindist
        self.minconn = minconn

    def SqDistance(self, x, y):
        result = 0
        for j in range(len(x)):
            result += (x[j] - y[j]) * (x[j] - y[j])
        return result

    def Fit(self, data, label):
        sqdist = self.mindist * self.mindist

        for j in range(len(label)):
            conn = 0
            for k in range(len(label)):
                if j != k and self.SqDistance(data[j], data[k]) < sqdist:
                    conn += 1
            if conn >= self.minconn:
                self.corepoints.append((label[j], data[j]))
            else:
                self.outlier.append(label[j])
        
        currcore = self.corepoints.copy()
        size = len(currcore)
        cluster = []
        core = None

        while (size > 0):
            cluster.append(currcore[0])
            j = 0
            while j < len(cluster):
                for point in currcore:
                    if point in cluster:
                        continue
                    if self.SqDistance(point[1], cluster[j][1]) < sqdist:
                        cluster.append(point)
                j += 1
            self.clusters.append([e[0] for e in cluster])
            size -= len(cluster)
            for item in cluster:
                currcore.remove(item)
            cluster = []        

class AssociationDiscovery:
    #Cuidado, valores pequenos de MinSupport e MinConfidence tornam o algoritimo
    #De tempo exponêncial

    MinSupport = 0.1 #Minimo valor de aderência ao número total T de transações
    MinConfidence = 0.3 #Minimo de confiança da eficiencia de uma regra
    MaxRules = 100
    rules = []

    #header: Lista de produtos como por exemplo: 
    #[queijo, arroz, leite, café]
    #data: Lista de transações, como por exemplo:
    #[
    #   [1, 0, 1, 0],
    #   [0, 1, 0, 1],
    #   [1, 1, 1, 1],
    #   [0, 0, 1, 1],
    #   [0, 1, 0, 0]
    #]
    #Onde 1 é a aderência ao produto e 0 é a não aderência

    #Treina sobre uma lista de produtos e transações. Retorna todas as regras obtidas
    def Fit(self, header, data):
        support = self.MinSupport * len(data)
        confidence = self.MinConfidence
        result = { }
        #Carrega todas as regras
        SetP = []
        for j in range(0, len(header)):
            SetP.append(j)
        self.SupportTest(SetP, data, support, confidence)

        self.rules.sort(key = self.getConf, reverse = True)

        result['Rules'] = self.rules
        result['Rules Count'] = len(self.rules)
        return result
    
    #Usado no método de ordenação
    def getConf(self, rule):
        return rule['Confidence']

    #Escreve regras na tela
    def PrintRules(self, header):
        print("Rules:")
        rultxt = ""
        txt = ""
        size = 0
        for r in self.rules:
            for x in r['X']:
                if txt != "":
                    txt += ","
                txt += header[x]
            rultxt += txt + " -> "
            txt = ""

            for y in r['Y']:
                if txt != "":
                    txt += ","
                txt += header[y]
            rultxt += txt
            txt = ""

            size = len(rultxt)
            rultxt += ((50 - size) * " ") + "Support: " + str(r['Support']) + "\tConfidence: " + str(r['Confidence'])
            print(rultxt)
            rultxt = ""

    #Testa o suporte de todas os subconjuntos de P possíveis
    def SupportTest(self, SetP, T, minsupp, minconf):
        #Retira produtos com baixo support
        optimizedSetP = []
        for p in SetP:
            if self.Supp([p], T, minsupp) != -1:
                optimizedSetP.append(p)
        SetP = optimizedSetP
        
        for i in range(1, len(SetP)):
            for j in range(i):
                C = [SetP[i], SetP[j]]
                self._SupportTest(C, self.diff(SetP, C), T, minsupp, minconf)
    
    #Conjunto menos elemento
    def remove(self, first, elem):
        return [item for item in first if item != elem]
    
    #Diferença de conjuntos
    def diff(self, first, sec):
        return [item for item in first if not item in sec]

    #Testa suporte recursivamente
    def _SupportTest(self, setMain, setP, T, minsupp, minconf):
        support = self.Supp(setMain, T, minsupp)
        if support == -1:
            return
        #Testa a confiança, dado que o suporte minimo foi alcançado
        self.ConfidenceTest(setMain, T, support, minconf)
        if len(setMain) < self.MaxRules:
            for p in setP:
                self._SupportTest(setMain + [p], self.remove(setP, p), T, minsupp, minconf)

    #Testa a confiança de todas as regras tal que SetP = X union Y
    def ConfidenceTest(self, setP, T, support, minconf):
        if len(setP) < 2:
            return
        for p in setP:
            self._ConfidenceTest(self.remove(setP, p), [p], T, support, minconf)
    
    #Testa a confiança recursivamente
    def _ConfidenceTest(self, X, Y, T, support, minconf):
        if X == []:
            return
        conf = self.Conf(X, T, support, minconf)
        if conf == -1:
            return
        self.rules.append({ 'X' : X, 'Y' : Y, 'Support' : support, 'Confidence' : conf})
        for p in X:
            self._ConfidenceTest(self.remove(X, p), Y + [p], T, support, minconf)

    #Support Limitado inferiormente
    def Supp(self, setP, T, minsupp):
        supp = 0
        for t in T:
            supp += 1
            for j in setP:
                if t[j] == 0.0:
                    supp -= 1
                    break
        if supp > minsupp:
            return supp
        return -1
    
    #Support Limitado superiormente
    def maxSupp(self, setP, T, maxsupp):
        supp = 0
        for t in T:
            supp += 1
            for j in setP:
                if t[j] == 0.0:
                    supp -= 1
                    break
            if supp > maxsupp:
                return -1
        return supp

    #Support Limitado inferiormente inverso
    def InvSupp(self, setP, T, minsupp):
        supp = 0
        for t in T:
            supp += 1
            for j in setP:
                if t[j] == 1.0:
                    supp -= 1
                    break
        if supp > minsupp:
            return supp
        return -1
    
    #Support Limitado superiormente inverso
    def InvmaxSupp(self, setP, T, maxsupp):
        supp = 0
        for t in T:
            supp += 1
            for j in setP:
                if t[j] == 1.0:
                    supp -= 1
                    break
            if supp > maxsupp:
                return -1
        return supp
    
    #Calcula Confidence
    def Conf(self, X, T, support, minconf):
        suppX = self.maxSupp(X, T, support / minconf)
        if suppX == -1:
            return -1
        else:
            return support / suppX

class Application:
    users = None
    friends = None
    artists = None
    likes = None
    movies = None

    def __init__(self):
        db = DataAccess()
        db.Connect()
        self.users = db.getUsers()
        self.friends = db.getFriendship()
        self.artists = db.getArtists()
        self.likes = db.getMusicalLikes()
        self.movies = db.getMovies()
        #INICIO DA SESSÂO: Fala sério Bruna
        _users = []
        for user in self.users:
            if user[0] not in ['Thanos', 'DeadPool', 'HomemDeFerro', 'DoutorEstranho', 'HomemAranha']:
                _users.append(user)
        self.users = _users
        #FIM DA SESSÂO: Fala sério Bruna
    
    def BuildGroupDataSet(self):
        #Criação do dataset
        header = [u[0] for u in self.users]
        dataset = []

        for user in header:
            item = [0 for u in header]
            friend = None
            for j in range(len(header)):
                friend = header[j]
                if friend == user:
                    item[j] += 1.0
                else:
                    if (friend, user) in self.friends:
                        item[j] += 0.5
                    if (user, friend) in self.friends:
                        item[j] += 0.5
            dataset.append(item)
    
        _dataset = []
        for j in range(len(dataset)):
            current = dataset[j].copy()
            for k in range(len(header)):
                if current[k] != 0:
                    for i in range(len(header)):
                        current[i] += dataset[k][i] * current[k] * 0.1
            _dataset.append(current)
        dataset = _dataset
    
        #Usando DBScan para agrupar pessoas
        model = DBScan(2.8, 1)
        model.Fit(dataset, header)
        groups = model.clusters + [[outlier] for outlier in model.outlier]
        return groups

    def MovieAssociation(self):
        groups = [x for x in self.BuildGroupDataSet() if len(x) > 1]
        dataset = []

        movieset = []
        movies = [(x[0], x[1]) for x in self.movies if x[2] > 3]
        for group in groups:
            tp = []
            for movie in movies:
                if movie[0] in group and not movie[1] in tp:
                    tp.append(movie[1])
            movieset.append(tp)
        
        header = []
        for x in movieset:
            for y in x:
                if not y in header:
                    header.append(y)
        
        for mset in movieset:
            ds = []
            for h in header:
                if h in mset:
                    ds.append(0)
                else:
                    ds.append(1)
            dataset.append(ds)
        
        model = AssociationDiscovery()
        model.MinConfidence = 0.8
        model.MinSupport = 0.8
        model.MaxRules = 2
        model.Fit(header, dataset)
        rules = model.rules
        return (header, rules)
    
    def RuleGraph(self):
        header, rules = self.MovieAssociation()
        G = nx.DiGraph()

        for h in header:
            G.add_node(h)

        for rule in rules:
           G.add_edge(header[rule['X'][0]], header[rule['Y'][0]])
        
        lay = nx.spring_layout(G)
        nx.draw_networkx_nodes(G, lay, cmap=plt.get_cmap('jet'), node_size = 500)
        nx.draw_networkx_labels(G, lay)
        nx.draw_networkx_edges(G, lay, edge_color='r', arrows=True)
        nx.draw_networkx_edges(G, lay, arrows=False)
        plt.show()

    def ClusterRuleGraph(self):
        header, rules = self.MovieAssociation()
        G = nx.DiGraph()
        
        result = []
        for j in range(len(header)):
            countX = 0
            countY = 0
            for rule in rules:
                if j in rule['X']:
                    countX += 1
                elif j in rule['Y']:
                    countY += 1
            if countX*countY > 150:
                result.append(j)
        
        #header = [self.ExtractMovie(x) for x in header]

        for rule in rules:
            if rule['X'][0] in result or rule['Y'][0] in result:
                G.add_edge(header[rule['X'][0]], header[rule['Y'][0]])
        
        lay = nx.spring_layout(G)
        nx.draw_networkx_nodes(G, lay, cmap=plt.get_cmap('jet'), node_size = 500)
        nx.draw_networkx_labels(G, lay)
        nx.draw_networkx_edges(G, lay, edge_color='r', arrows=True)
        nx.draw_networkx_edges(G, lay, arrows=False)
        plt.show()
        
        rset = []
        for r in result:
            rset.append(header[r])
        rset = [self.ExtractMovie(x)[0] for x in rset]
        return rset
    
    def ExtractMovie(self, id):
        nome = "sem nome"
    
        try:
            wiki = "https://www.imdb.com/title/" + str(id)                              # especificando a url o URL
            page = urllib.request.urlopen(wiki)
            soup = BeautifulSoup(page.read(), 'html.parser')                            # agora soup tem o html da página
        
            nome = soup.find('div', class_='title_wrapper').text.strip().split('\n')[0].replace('\xa0', ' ')
        except Exception as e:
                print(e)
        return nome

App = Application()
App.RuleGraph()