# -*- coding: utf-8 -*-
#!/usr/bin/python
import psycopg2
import matplotlib.pyplot as plt

class BancoDeDados:
    # construtor em python, self para ela mesma
    def __init__(self, dbname="1901Banqueta_de_Cubos", user="1901Banqueta_de_Cubos", password="899675", host="200.134.10.32"): 
        self.dbname     = dbname
        self.user       = user
        self.password   = password
        self.host       = host

    def conectarBancoDeDados(self):
        try:
            self.conexao = psycopg2.connect(dbname=self.dbname, user=self.user, password=self.password, host=self.host)
            self.cursor = self.conexao.cursor()
            print("Conectado ao banco de dados: " + self.dbname + " :)")
        except:
            print("Não foi possível conectar ao banco de dados: " + self.dbname + " :(")
        
    def executar(self, comando):
        try:
            self.cursor.execute(comando)
            for tabela in self.cursor.fetchall():
                print tabela
        except Exception as e:
            print(e)
        self.conexao.commit()

    def transformaListaEmStringAceitavelPeloBancoDeDados(self, lista):
        print lista
        string_aceitavel = ""
        for atributo in lista:
            if atributo == lista[-1]:
                string_aceitavel += "'" + atributo + "'"
            else:
                string_aceitavel += "'" + atributo + "',"
        return string_aceitavel

    def inserirEmTabela(self, nome_tabela, vetor_atributos):
        atributos = self.transformaListaEmStringAceitavelPeloBancoDeDados(vetor_atributos)
        inserir = "INSERT INTO " + nome_tabela + " VALUES (" + atributos + ");"
        self.executar(inserir)
        
    def listarDeTabela(self, nome_atributo, nome_tabela):
        listar = "SELECT "+ nome_atributo + " FROM " + nome_tabela + ";"
        self.executar(listar)
        
    def alterarDadoDeTabela(self, nome_tabela, nome_coluna, atributo_condicao, id, novo_atributo):
        alterar = "UPDATE " + str(nome_tabela) + " SET " + str(nome_coluna) + " = '" + str(novo_atributo) + "' WHERE " + str(atributo_condicao) + " = '" + str(id) + "';"
        self.executar(alterar)

    def deletarLinhaDeTabela(self, nome_tabela, nome_coluna_condicao, condicao):
        alterar = "DELETE FROM " + str(nome_tabela) + " WHERE " + str(nome_coluna_condicao) + " = '" + str(condicao) + "';"
        self.executar(alterar)
    
    def deletarAmizade(self, login1, login2):
        deletar = "DELETE FROM registra WHERE login1 = '" + str(login1) + "' AND login2 = '" + str(login2) + "';"
        self.executar(deletar)
        
    def tiraDesvioPadraoDeColunaDaTabela(self, nome_tabela, nome_atributo):
        desvio = "SELECT STDDEV(" + nome_atributo + ") FROM " + nome_tabela
        self.executar(desvio)

    def tiraMediaDeColunaDaTabela(self, nome_tabela, nome_atributo):
        desvio = "SELECT AVG(" + nome_atributo + ") FROM " + nome_tabela
        self.executar(desvio)

    def fazGraficoDeTabelaApartirDeDoisAtributos(self, nome_tabela, atributo_x, atributo_y, legenda_x, legenda_y):
        valores = []
        eixo_x = []
        eixo_y = []
        for i in xrange(0,20):
            comando = "WITH tabela AS (SELECT "+str(atributo_x)+", COUNT("+str(atributo_y)+") AS coluna FROM "+str(nome_tabela)+" GROUP BY "+str(atributo_x)+") SELECT COUNT("+str(atributo_x)+") FROM tabela WHERE coluna="+str(i)+";"
            #comando = "WITH tabela AS (SELECT "+str(atributo_x)+", COUNT("+str(atributo_y)+") AS coluna FROM "+str(nome_tabela)+" GROUP BY "+str(atributo_x)+") SELECT COUNT("+str(atributo_x)+") FROM tabela WHERE coluna="+str(valor)+";"
            try:
                self.cursor.execute(comando)
                for tabela in self.cursor.fetchall():
                    valores.append(tabela)
                eixo_x.append(i)
            except Exception as e:
                print(e)
            self.conexao.commit()
        for i in xrange(0,20):
            eixo_y.append(valores[i][0])
        plt.bar(eixo_x, eixo_y, color='pink')
        plt.xticks(eixo_x)
        plt.xlabel(legenda_x)
        plt.ylabel(legenda_y)
        plt.show()
        
    def funcaoQueCalculaMelhoresArtistasMusicaisEFilmesComBaseNaAvaliacaoDoUsuarioQueUsaSeisFuckingSelects(self):
        comando = """SELECT f.Id as Codigo, (SELECT AVG(cf2.nota) FROM curte_filme cf2 Where cf2.id_filme = f.id) as Nota
        FROM filme f
        WHERE (SELECT COUNT(*) FROM curte_filme cf Where cf.id_filme = f.id) > 2
        UNION 
        SELECT ar.Id as Codigo, (SELECT AVG(cf2.nota) FROM curte_artista_musical cf2 Where cf2.id_artista = ar.id) as Nota
        FROM ArTiStA_MuSiCaL ar
        WHERE (SELECT COUNT(*) FROM curte_artista_musical cf Where cf.id_artista = ar.id) > 2
        ORDER BY Nota DESC
        LIMIT 10"""
        self.executar(comando)
        
    def dezAtributosMaisPopulares(self):
        comando = """SELECT f.Id as Identificador, (SELECT COUNT(*) FROM curte_filme cf where cf.id_filme = f.id) as Popularidade FROM filme f
        UNION
        SELECT am.Id as Identificador, (SELECT COUNT(*) FROM curte_artista_musical ca where ca.id_artista = am.id) as Popularidade FROM artista_musical am
        order by Popularidade desc
        LIMIT 10"""
        self.executar(comando)

    def dezAtributosMaenosPopulares(self):
        comando = """SELECT f.Id as Identificador, (SELECT COUNT(*) FROM curte_filme cf where cf.id_filme = f.id) as Popularidade FROM filme f
        UNION
        SELECT am.Id as Identificador, (SELECT COUNT(*) FROM curte_artista_musical ca where ca.id_artista = am.id) as Popularidade FROM artista_musical am
        order by Popularidade asc
        LIMIT 11"""
        self.executar(comando)

    def criaViewConheceNormalizada(self):
        comando = """CREATE VIEW ConheceNormalizada  AS 
			SELECT login1, login2
			FROM registra UNION 
			SELECT login2, login1 
			FROM registra;"""
        self.executar(comando)

    def pessoasEmConheceNormalizadaQueCompartilhamOMaiorNumeroDeFilmesCurtidos(self):
        comando = """SELECT cn.login1, cn.login2 FROM conheceNormalizada cn
        WHERE (SELECT COUNT(cf1.id_filme) FROM curte_filme cf1, curte_filme cf2, conheceNormalizada cn2
        WHERE cn.login1=cf1.login AND cn.login2=cf2.login AND cf1.id_filme=cf2.id_filme)>0
        ORDER BY (SELECT COUNT(cf1.id_filme) FROM curte_filme cf1, curte_filme cf2
        WHERE cn.login1=cf1.login AND cn.login2=cf2.login AND cf1.id_filme=cf2.id_filme) DESC LIMIT 1"""
        self.executar(comando)
    
    def numeroDeConhecidosDosConhecidosDeCadaIntegranteDoGrupo(self, login):
        comando = """SELECT COUNT (cn2.login2)
            FROM conheceNormalizada cn1, conheceNormalizada cn2
            WHERE cn1.login1='"""+str(login)+"""' AND cn2.login1=cn1.login2 AND cn1.login2=cn2.login1"""
        self.executar(comando)

    def nomeDosConhecidosDosConhecidosDeCadaIntegranteDoGrupo(self, login):
        comando = """SELECT cn2.login2
            FROM conheceNormalizada cn1, conheceNormalizada cn2
            WHERE cn1.login1='"""+str(login)+"""' AND cn2.login1=cn1.login2 AND cn1.login2=cn2.login1"""
        self.executar(comando)



def menu():
    banquetaDeCubos = BancoDeDados()
    banquetaDeCubos.conectarBancoDeDados()

    # 1 - Qual é a média e desvio padrão dos ratings para artistas musicais e filmes?
    #banquetaDeCubos.tiraMediaDeColunaDaTabela('curte_artista_musical','nota')
    #banquetaDeCubos.tiraDesvioPadraoDeColunaDaTabela('curte_artista_musical','nota')
    #banquetaDeCubos.tiraMediaDeColunaDaTabela('curte_filme', 'nota')
    #banquetaDeCubos.tiraDesvioPadraoDeColunaDaTabela('curte_filme', 'nota')

    # 2 - Quais são os artistas e filmes com o maior rating médio curtidos por pelo menos duas pessoas? Ordenados por rating médio.
    #banquetaDeCubos.funcaoQueCalculaMelhoresArtistasMusicaisEFilmesComBaseNaAvaliacaoDoUsuarioQueUsaSeisFuckingSelects()

    # 3 - Quais são os 10 artistas musicais e filmes mais populares? Ordenados por popularidade.
    #banquetaDeCubos.dezAtributosMaisPopulares()

    # 4 - Crie uma view chamada ConheceNormalizada que represente simetricamente os relacionamentos de conhecidos da turma. Por exemplo, se a conhece b mas b não declarou conhecer a, a view criada deve conter o relacionamento (b,a) além de (a,b).
    #banquetaDeCubos.criaViewConheceNormalizada()    

    # 5 - Quais são os conhecidos (duas pessoas ligadas na view ConheceNormalizada) que compartilham o maior numero de filmes curtidos?
    banquetaDeCubos.pessoasEmConheceNormalizadaQueCompartilhamOMaiorNumeroDeFilmesCurtidos()                        

    # 6 - Qual o número de conhecidos dos conhecidos (usando ConheceNormalizada) para cada integrante do seu grupo?
    #banquetaDeCubos.numeroDeConhecidosDosConhecidosDeCadaIntegranteDoGrupo('brunapinheiro')
    #banquetaDeCubos.numeroDeConhecidosDosConhecidosDeCadaIntegranteDoGrupo('leonardosilio')
    #banquetaDeCubos.numeroDeConhecidosDosConhecidosDeCadaIntegranteDoGrupo('lucasrech')
    
    # 7 - Construa um gráfico para a função f(x) = (número de pessoas que curtiram exatamente x filmes).
    #banquetaDeCubos.fazGraficoDeTabelaApartirDeDoisAtributos('curte_filme','login','id_filme',"x filmes","numero de pessoas que curtixam x filmes")
    
    # 8 - Construa um gráfico para a função f(x) = (número de filmes curtidos por exatamente x pessoas).
    #banquetaDeCubos.fazGraficoDeTabelaApartirDeDoisAtributos('curte_filme','id_filme','login',"x pessoas","numero de filmes curtidos por x pessoas")
    
    # 9 - Defina duas outras informações (como as anteriores) que seriam úteis para compreender melhor a rede. Agregue estas informações à sua aplicação.
    # 9.1 - Quais os nomes dos conhecidos dos conhecidos (usando ConheceNormalizada) para um integrante do seu grupo?
    #banquetaDeCubos.nomeDosConhecidosDosConhecidosDeCadaIntegranteDoGrupo('lucasrech')
    # 9.2 - Quais são os 10 artistas musicais e filmes menos populares? Ordenados por menor popularidade.
    #banquetaDeCubos.dezAtributosMaenosPopulares()




def main():
    menu()

if __name__ == '__main__':
    main()
