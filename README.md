# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados


## Integrantes do grupo - Banqueta de Cubos

- Bruna Araújo Pinheiro, 1904930, brunaap
- Lucas Coradin Rech, 1378945, salazarcr
- Leonardo Trevisan Silio, 1936379, TrevisanPhi

## Descrição da aplicação a ser desenvolvida 

- O que vocês vão implementar

    Vamos implementar um algoritmo que agrupa as pessoas por afinidade a fim de estudar a composição de grupos. Futuramente, também planejamos tentar usar esse algoritmo para determinar a relação entre as notas de um estudande e seus gostos pessoais.

- Quais tecnologias vão utilizar

    Python, DBScan, Association Discovery, entre outros algoritmos, além de psycopg2 e programas para a visualisação de grafos.

- Por que decidiram pelo tema

    Porque achamos interessantes realizar um estudo sobre os grupos dentro da sala de aula, como eles são formados e o que influencia nessa formação.

- Quais os desafios relacionados com bancos de dados (consultas, modelos, etc.)

   Por conta do processamento estar sendo feito na aplicação não há dificuldades significativas nas consultas.

- Quais os desafios relacionados com suas áreas de interesse

    Fazer a melhor escolha possível dos algoritmos de Machine Learning, entender como eles funcionam os significados estatísticos de cada resultado.

- Quais conhecimentos novos vocês pretendem adquirir

    Pretendemos adquirir onhecimentos sobre ciência de dados, Machine Learning e seus algoritmos em geral.

- O que vocês já produziram (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.)

    Produzimos a relação dos grupos de pessoas que possuem maior afinidade.

```
- Nome: Bruna Araújo Pinheiro
- Qual o seu foco na implementação do trabalho?
    Aprofundar meus conhecimentos sobre as tecnologias empregadas na implementação do trabalho e ajudar a equipe da melhor forma que conseguir.
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
    Sim, já ví palestras e participaei de minicursos sobre algumas das tecnologias.
- Qual aspecto do trabalho te interessa mais?
    O estudo das preferências das pessoas e como isso pode interferir na formação de grupos.
```

```
- Nome: Leonardo Trevisan Silio
- Qual o seu foco na implementação do trabalho?
    Avaliação dos algoritmos e estudos a respeito de estatística de testes de várias técnicas de ML.
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
    Sim, faço parte de um estágio com enfoque em Machine Learning, por isso, tenho uma boa noção, assim como tenho uma boa noção de python.
- Qual aspecto do trabalho te interessa mais?
    As técnicas empregadas na ciência de dados.
```

```
- Nome: Lucas Coradin Rech
- Qual o seu foco na implementação do trabalho?
    Aprender a utilizar as tecnologias usadas no trabalho.
- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
    Não.
- Qual aspecto do trabalho te interessa mais?
    Utilização de grafos para o estudo comportamental das pessoas.
```


