# -*- coding: utf-8 -*-
#!/usr/bin/python
from xml.dom import minidom
import urllib2
import psycopg2

class BancoDeDados:
    # construtor em python, self para ela mesma
    def __init__(self, dbname="1901Banqueta_de_Cubos", user="1901Banqueta_de_Cubos", password="899675", host="200.134.10.32"): 
        self.dbname     = dbname
        self.user       = user
        self.password   = password
        self.host       = host

    def conectarBancoDeDados(self):
        try:
            self.conexao = psycopg2.connect(dbname=self.dbname, user=self.user, password=self.password, host=self.host)
            self.cursor = self.conexao.cursor()
            print("Conectado ao banco de dados: " + self.dbname + " :)")
        except:
            print("Não foi possível conectar ao banco de dados: " + self.dbname + " :(")

    def inserirEmTabela(self, nome_tabela, vetor_atributos):
        atributos = parse(vetor_atributos)
        inserir = "INSERT INTO " + nome_tabela + " VALUES (" + atributos + ");"
        try:
            self.cursor.execute(inserir)
        except Exception as e:
            print "Nao foi possivel inserir" + atributos + " em " + str(nome_tabela)
            print(e)
        self.conexao.commit()


def fazParsingDeXmlDaInternet(url, tag_name, atributos):
    # pegando os dados das pessoas
    tira1 = 'http://utfpr.edu.br/CSB30/2019/1/DI1901'
    tira2 = 'http://www.imdb.com/title/'
    tira3 = 'https://en.wikipedia.org/wiki/'
    aux=[]
    resultado=[]
    dom = minidom.parse(urllib2.urlopen(url)) # pega o xml da internet
    tag = dom.getElementsByTagName(tag_name) # pega tudo que começa com link
    for items in tag:
        aux=[]
        for i in xrange(0, len(atributos)):
            if items.attributes[atributos[i]]:
                string = items.attributes[atributos[i]].value
                if( tira1 in string ):
                    string = string.replace(tira1, "")
                elif(tira2 in string):
                    string = string.replace(tira2, "")
                elif(tira3 in string):
                    string = string.replace(tira3, "")
                aux.append(string)
        resultado.append(aux)
    return resultado # retorna as linhas do xml

def main():
    banquetaDeCubos = BancoDeDados()
    banquetaDeCubos.conectarBancoDeDados()
    
    # dados do xml de pessoas
    url_person = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml'
    tag_person = 'Person'
    atributos_person = ['uri', 'name', 'hometown', 'birthdate']
    resultado_person = fazParsingDeXmlDaInternet(url_person, tag_person, atributos_person)
    for i in xrange(0, len(resultado_person)):
        banquetaDeCubos.inserirEmTabela('usuario', resultado_person[i])

    # dados do xml de conhecidos
    url_conhecidos = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml'
    tag_conhecidos = 'Knows'
    atributos_conhecidos = ['person', 'colleague']
    resultado_conhecidos = fazParsingDeXmlDaInternet(url_conhecidos, tag_conhecidos, atributos_conhecidos)
    for i in xrange(0, len(resultado_conhecidos)):
        banquetaDeCubos.inserirEmTabela('registra', resultado_conhecidos[i])

    # dados do xml de curtir filmes
    url_filme = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml'
    tag_filme = 'LikesMovie'
    atributos_filme = ['person', 'rating', 'movieUri']
    resultado_filme = fazParsingDeXmlDaInternet(url_filme, tag_filme, atributos_filme)
    for i in xrange(0, len(resultado_filme)):
        banquetaDeCubos.inserirEmTabela('filme', [resultado_filme[i][2],'','','','',''] )
    for i in xrange(0, len(resultado_filme)):
        banquetaDeCubos.inserirEmTabela('curte_filme', [resultado_filme[i][0], resultado_filme[i][2], resultado_filme[i][1]] )

    # dados do xml de musicos
    url_musico = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml'
    tag_musico = 'LikesMusic'
    atributos_musico = ['person', 'rating', 'bandUri']
    resultado_musico = fazParsingDeXmlDaInternet(url_musico, tag_musico, atributos_musico)
    for i in xrange(0, len(resultado_musico)):
        banquetaDeCubos.inserirEmTabela('artista_musical', [resultado_musico[i][2],'','',''] )
    for i in xrange(0, len(resultado_musico)):
        banquetaDeCubos.inserirEmTabela('curte_artista_musical', [resultado_musico[i][0], resultado_musico[i][2], resultado_musico[i][1] ] )


if __name__ == '__main__':
    main()