#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.dom.minidom import parse
import xml.dom.minidom
import csv
import os

# verificando se o diretório existe para cria-lo
dir = 'dadosMarvel/'
if not os.path.exists(dir):
   os.mkdir(dir)

#criando  e limpando os arquivos "herois_good.csv”, "herois_bad.csv” e "herois.csv"
herois = open(dir + 'herois.csv', 'w') 
herois_good = open(dir + 'herois_good.csv', 'w')
herois_bad = open(dir + 'herois_bad.csv', 'w')

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("/home/bruna/Documentos/banco_de_dados/xml-java-python/marvel_simplificado.xml")

universe = DOMTree.documentElement

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

#variáveis para os cálculos
good = 0.
bad = 0.
peso = 0.

# Print detail of each hero.
for hero in heroes:
   if hero.hasAttribute("id"):
     linha = str(hero.getAttribute("id")) # aqui a linha já é zerada automaticamente

   name = hero.getElementsByTagName('name')[0]
   linha = linha + ", " + str(name.childNodes[0].data)
   popularity = hero.getElementsByTagName('popularity')[0]
   linha = linha + ", " + str(popularity.childNodes[0].data)
   alignment = hero.getElementsByTagName('alignment')[0]
   linha = linha + ", " + str(alignment.childNodes[0].data)
   gender = hero.getElementsByTagName('gender')[0]
   linha = linha + ", " + str(gender.childNodes[0].data)
   height_m = hero.getElementsByTagName('height_m')[0]
   linha = linha + ", " + str(height_m.childNodes[0].data)
   weight_kg = hero.getElementsByTagName('weight_kg')[0]
   linha = linha + ", " + str(weight_kg.childNodes[0].data)
   hometown = hero.getElementsByTagName('hometown')[0]
   linha = linha + ", " + str(hometown.childNodes[0].data)
   intelligence = hero.getElementsByTagName('intelligence')[0]
   linha = linha + ", " + str(intelligence.childNodes[0].data)
   strength = hero.getElementsByTagName('strength')[0]
   linha = linha + ", " + str(strength.childNodes[0].data)
   speed = hero.getElementsByTagName('speed')[0]
   linha = linha + ", " + str(speed.childNodes[0].data)
   durability = hero.getElementsByTagName('durability')[0]
   linha = linha + ", " + str(durability.childNodes[0].data)
   energy_Projection = hero.getElementsByTagName('energy_Projection')[0]
   linha = linha + ", " + str(energy_Projection.childNodes[0].data)
   fighting_Skills = hero.getElementsByTagName('fighting_Skills')[0]
   linha = linha + ", " + str(fighting_Skills.childNodes[0].data) + "\n"

   # escrevendo a linhas nos respectivos arquivos
   herois.writelines(linha)
   if(alignment.childNodes[0].data == 'Good'):
       herois_good.writelines(linha)
       good = good + 1
   if(alignment.childNodes[0].data == 'Bad'):
       herois_bad.writelines(linha) 
       bad = bad + 1
   if(name.childNodes[0].data == 'Hulk'):
       hulk_peso = float(weight_kg.childNodes[0].data)
       hulk_altura = float(height_m.childNodes[0].data)
   peso = peso + float(weight_kg.childNodes[0].data)

# proporção de heróis bons/maus
print "Proporção de heróis bons/maus = " + str(good/bad)

# média de peso dos heróis
print "Média de peso dos heróis = " + str(peso/float(hero.getAttribute("id"))) # o último ID informa o número de heróis

# índice de massa corporal do Hulk
print "Índice de massa corporal do Hulk = " + str(hulk_peso/(hulk_altura*hulk_altura))

# fechando os arquivos
herois.close()
herois_good.close()
herois_bad.close()


