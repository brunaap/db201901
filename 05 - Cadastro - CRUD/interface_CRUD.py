# -*- coding: utf-8 -*-
#!/usr/bin/python
import psycopg2

class BancoDeDados:
    # construtor em python, self para ela mesma
    def __init__(self, dbname="1901Banqueta_de_Cubos", user="1901Banqueta_de_Cubos", password="899675", host="200.134.10.32"): 
        self.dbname     = dbname
        self.user       = user
        self.password   = password
        self.host       = host

    def conectarBancoDeDados(self):
        try:
            self.conexao = psycopg2.connect(dbname=self.dbname, user=self.user, password=self.password, host=self.host)
            self.cursor = self.conexao.cursor()
            print("Conectado ao banco de dados: " + self.dbname + " :)")
        except:
            print("Não foi possível conectar ao banco de dados: " + self.dbname + " :(")

    def transformaListaEmStringAceitavelPeloBancoDeDados(self, lista):
        print lista
        string_aceitavel = ""
        for atributo in lista:
            if atributo == lista[-1]:
                string_aceitavel += "'" + atributo + "'"
            else:
                string_aceitavel += "'" + atributo + "',"
        return string_aceitavel

    def inserirEmTabela(self, nome_tabela, vetor_atributos):
        atributos = self.transformaListaEmStringAceitavelPeloBancoDeDados(vetor_atributos)
        inserir = "INSERT INTO " + nome_tabela + " VALUES (" + atributos + ");"
        try:
            self.cursor.execute(inserir)
        except Exception as e:
            print "Nao foi possivel inserir" + atributos + " em " + str(nome_tabela)
            print(e)
        self.conexao.commit()
        
    def listarDeTabela(self, nome_tabela):
        listar = "SELECT * FROM " + nome_tabela + ";"
        try:
            self.cursor.execute(listar)
            for tabela in self.cursor.fetchall():
                print tabela
        except Exception as e:
            print "Nao foi possivel listar dados da tabela " + str(nome_tabela)
            print(e)
        self.conexao.commit()
        
    def alterarDadoDeTabela(self, nome_tabela, nome_coluna, atributo_condicao, id, novo_atributo):
        alterar = "UPDATE " + str(nome_tabela) + " SET " + str(nome_coluna) + " = '" + str(novo_atributo) + "' WHERE " + str(atributo_condicao) + " = '" + str(id) + "';"
        try:
            self.cursor.execute(alterar)
        except Exception as e:
            print "Nao foi possivel alterar " + str(novo_atributo) + " na coluna " + str(nome_coluna) + " da tabela " + str(nome_tabela)
            print(e)
        self.conexao.commit()

    def deletarLinhaDeTabela(self, nome_tabela, nome_coluna_condicao, condicao):
        alterar = "DELETE FROM " + str(nome_tabela) + " WHERE " + str(nome_coluna_condicao) + " = '" + str(condicao) + "';"
        try:
            self.cursor.execute(alterar)
        except Exception as e:
            print "Nao foi possivel deletar " + str(condicao) + "na coluna" + str(nome_coluna_condicao) + " da tabela " + str(nome_tabela)
            print(e)
        self.conexao.commit()
    
    def deletarAmizade(self, login1, login2):
        deletar = "DELETE FROM registra WHERE login1 = '" + str(login1) + "' AND login2 = '" + str(login2) + "';"
        try:
            self.cursor.execute(deletar)
        except Exception as e:
            print "Não foi possivel deletar sua amizade, vocês devem ser amigos pela vida toda :D"
            print(e)
        self.conexao.commit()


def menu():
    banquetaDeCubos = BancoDeDados()
    banquetaDeCubos.conectarBancoDeDados()
    opcao = '0'
    dados = []
    while(opcao!='3'):
        if(opcao=='0'):
            print "Selecione a opção desejada:"
            print "  1 - Listar usuários"
            print "  2 - Cadastrar usuários"
            print "  3 - Sair"
            opcao = raw_input('  Opção: ')
        elif(opcao=='1'):
            banquetaDeCubos.listarDeTabela('usuario')
            print "  4 - Editar usuário"
            print "  5 - Apagar usuário"
            print "  6 - Voltar"
            opcao = raw_input('  Opção: ')
        elif(opcao=='2'):
            login = raw_input("  Login: ")
            nome_completo = raw_input("  Nome completo: ")
            cidade_natal = raw_input("  Cidade natal: ")
            data_de_nascimento = raw_input("  Data de nascimento: ")
            dados.append(login)
            dados.append(nome_completo)
            dados.append(cidade_natal)
            dados.append(data_de_nascimento)
            banquetaDeCubos.inserirEmTabela('usuario', dados)
            dados = []
            opcao = '0'
        elif(opcao=='4'):
            print "  7 - Editar dados de um usuário"
            print "  8 - Apagar amizade"
            print "  9 - Adicionar amizade"
            print "  10 - Voltar"
            opcao = raw_input('  Opção: ')
        elif(opcao=='5'):
            login = raw_input("  Login do usuario a ser deletado: ")
            banquetaDeCubos.deletarLinhaDeTabela('usuario','login',login)
            opcao = '0'
        elif(opcao=='5'):
            login = raw_input("  Login do usuario a ser deletado: ")
            banquetaDeCubos.deletarLinhaDeTabela('usuario','login',login)
            opcao = '0'
        elif(opcao=='6' or opcao=='10'):
            opcao = '0'
        elif(opcao=='7'):
            login = raw_input("  Login do usuário a ser alterado: ")
            coluna = raw_input("  Coluna a ser alterada: ")
            novo_atributo = raw_input("  Novo atributo: ")
            banquetaDeCubos.alterarDadoDeTabela('usuario',coluna,'login',login,novo_atributo)
            opcao = '0'
        elif(opcao=='8'):
            login1 = raw_input("  Login do primeiro usuario: ")
            login2 = raw_input("  Login do segundo usuario: ")
            banquetaDeCubos.deletarAmizade(login1, login2)
            opcao = '0'
        elif(opcao=='9'):
            login1 = raw_input("  Login do primeiro usuario: ")
            login2 = raw_input("  Login do segundo usuario: ")
            dados.append(login1)
            dados.append(login2)
            banquetaDeCubos.inserirEmTabela('registra',dados)
            dados=[]
            opcao = '0'



def main():
    menu()



if __name__ == '__main__':
    main()
