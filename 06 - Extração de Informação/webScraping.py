# -*- coding: utf-8 -*-
#!/usr/bin/python
import psycopg2                     # para mexer com o banco de dados
import urllib2                      # importe a biblioteca usada para consultar uma URL
from bs4 import BeautifulSoup       # importe as funções BeautifulSoup para analisar os dados retornados do site
from lxml import etree              # para editar os dados xml


class BancoDeDados:
    # construtor em python, self para ela mesma
    def __init__(self, dbname="1901Banqueta_de_Cubos", user="1901Banqueta_de_Cubos", password="899675", host="200.134.10.32"): 
        self.dbname     = dbname
        self.user       = user
        self.password   = password
        self.host       = host

    def conectarBancoDeDados(self):
        try:
            self.conexao = psycopg2.connect(dbname=self.dbname, user=self.user, password=self.password, host=self.host)
            self.cursor = self.conexao.cursor()
            print("Conectado ao banco de dados: " + self.dbname + " :)")
        except:
            print("Não foi possível conectar ao banco de dados: " + self.dbname + " :(")
        
    def executar(self, comando):
        try:
            resultado = []
            self.cursor.execute(comando)
            for tabela in self.cursor.fetchall():
                resultado.append(tabela)
            return(resultado)
        except Exception as e:
            print(e)
        self.conexao.commit()

    def inserirEmTabela(self, nome_tabela, vetor_atributos):
        atributos = parse(vetor_atributos)
        inserir = "INSERT INTO " + nome_tabela + " VALUES (" + atributos + ");"
        self.executar(inserir)
        
    def listarDeTabela(self, nome_atributo, nome_tabela):
        listar = "SELECT "+ nome_atributo + " FROM " + nome_tabela + ";"
        return self.executar(listar)
        
    def alterarDadoDeTabela(self, nome_tabela, nome_coluna, atributo_condicao, id, novo_atributo):
        alterar = "UPDATE " + str(nome_tabela) + " SET " + str(nome_coluna) + " = '" + str(novo_atributo) + "' WHERE " + str(atributo_condicao) + " = '" + str(id) + "';"
        self.executar(alterar)

    def deletarLinhaDeTabela(self, nome_tabela, nome_coluna_condicao, condicao):
        alterar = "DELETE FROM " + str(nome_tabela) + " WHERE " + str(nome_coluna_condicao) + " = '" + str(condicao) + "';"
        self.executar(alterar)
    
    def deletarAmizade(self, login1, login2):
        deletar = "DELETE FROM registra WHERE login1 = '" + str(login1) + "' AND login2 = '" + str(login2) + "';"
        self.executar(deletar)

def trocaGeral(vetor, caractere):
    vetor = (vetor).replace(caractere,',')
    vetor = (vetor).replace('0',',')
    vetor = (vetor).replace('1',',')
    vetor = (vetor).replace('2',',')
    vetor = (vetor).replace('3',',')
    vetor = (vetor).replace('4',',')
    vetor = (vetor).replace('5',',')
    vetor = (vetor).replace('6',',')
    vetor = (vetor).replace('7',',')
    vetor = (vetor).replace('8',',')
    vetor = (vetor).replace('9',',')
    vetor = (vetor).replace(')',',')
    vetor = (vetor).replace('(',',')
    vetor = (vetor).replace('-',',')
    vetor = vetor.split(',')
    vetor.remove('')
    return vetor

def pegaCidadeEstadoPais(vetor):
    tam = len(vetor)
    cidade = 'sem dados'
    estado = 'sem dados'
    pais = 'sem dados'
    if tam >= 3:
        cidade = vetor[tam-3]
        estado = vetor[tam-2]
        pais = vetor[tam-1]
    elif tam >= 2:
        cidade = vetor[tam-2]
        pais = vetor[tam-1]        
    return cidade, estado, pais

def trocaMes(data):
    data = data.replace('January','-01-')
    data = data.replace('February','-02-')
    data = data.replace('March','-03-')
    data = data.replace('April','-04-')
    data = data.replace('May','-05-')
    data = data.replace('June','-06-')
    data = data.replace('July','-07-')
    data = data.replace('August','-08-')
    data = data.replace('September','-09-')
    data = data.replace('October','-10-')
    data = data.replace('November','-11-')
    data = data.replace('December','-12-')
    return data

def extraiDaWikiArtista(id):
    genero = []
    cidade = 'sem dados'
    pais = 'sem dados'
    nome = 'sem nome'

    chaves_origem = ['Origin','Born']
    chaves_genero = ['Genres']

    try:
        wiki = "https://en.wikipedia.org/wiki/" + str(id)                          # especificando a url o URL
        page = urllib2.urlopen(wiki)                                               # consultando o site e retornando o html para a variável 'page'
        soup = BeautifulSoup(page, 'html5lib')                                     # agora soup tem o html da página
        
        nome = (soup.find('h1', class_='firstHeading')).text.strip()               # extrai o nome do artista

        infobox = soup.find('table', class_='infobox')                             # pegando o infobox
        linhas_infobox = infobox.find_all('tr')                                    # pegando as linhas do infobox
        for tr in linhas_infobox:                                               
            for chaveg in chaves_genero:                                           # pegando o gênero do infobox
                if(chaveg in tr.text):
                    genero = (tr.text).replace(' ',',')
                    genero = trocaGeral(genero,chaveg)
            for chaveo in chaves_origem:                                           # pegando a origem do infobox
                if(chaveo in tr.text):
                    origem = trocaGeral(tr.text,chaveo)
                    cidade, estado, pais = pegaCidadeEstadoPais(origem)
    except Exception as e:
            print(e)
    return nome, genero, cidade, pais

def extraiDaWikiFilme(id):
    receita = 'US$ nada'
    try:
        id = id.replace(' ','_')                                                   # tratando o nome para ser reconhecido pelo site
        wiki = "https://pt.wikipedia.org/wiki/" + str(id)                          # especificando a url o URL
        page = urllib2.urlopen(wiki)                                               # consultando o site e retornando o html para a variável 'page'
        soup = BeautifulSoup(page, 'html5lib')                                     # agora soup tem o html da página

        infobox = soup.find('table', class_='infobox_v2')                             # pegando o infobox
        linhas_infobox = infobox.find_all('tr')                                    # pegando as linhas do infobox
        for tr in linhas_infobox:                                               
            if('Receita' in tr.text):                                              # pegando a receita do filme
                receita = ((tr.text).replace('Receita','')).replace('\n','')
                receita = (receita.split('['))[0]
                if '(' in receita:
                    receita = (receita.split('(')[1]).split(')')[0]
    except Exception as e:
            print(e)
    return receita


def extraiDoImdb(id):
    nome = "sem nome"
    genero = "sem genero"
    data = "Sem data"
    diretor = "Sem diretor"
    
    try:
        imdb = "https://www.imdb.com/title/" + str(id)                             # especificando a url o URL
        page = urllib2.urlopen(imdb)                                               # consultando o site e retornando o html para a variável 'page'
        soup = BeautifulSoup(page, 'html5lib')                                     # agora soup tem o html da página
        
        nome = (soup.find('div', class_='title_wrapper')).text.strip()             # extrai o nome do filme
        nome = (((nome.replace('(','\n')).split('\n'))[0]).strip()                 # tratando os dados recebidos, strip para tirar o \xa0
        
        diretor = soup.find('div', class_='credit_summary_item').text.strip()      # estrai o nome do diretor
        diretor = (diretor.split('\n'))[1]                                         # tratando os dados recebidos

        dados = soup.find('div', class_='subtext').text.strip()
        dados = dados.split('\n')
        del(dados[0:5])

        data = dados.pop(len(dados)-1)
        data = ((data.replace(' ','')).split("("))[0]
        data = trocaMes(data)

        del(dados[len(dados)-1])
        genero = "".join(dados)

    except Exception as e:
            print(e)
    return nome, genero, data, diretor

def extraiDolastfm(id):
    popular = 'nao cadastrado'
    # fazendo um tratamento da id do artista
    id = id.replace('_(',',')
    id = id.replace('(',',')
    id = id.replace('_','+')
    id = id.split(',')
    # pegando a música mais popular da semana para esse artista
    try:
        site = "https://www.last.fm/music/" + str(id[0])                            # especificando a url o URL
        page = urllib2.urlopen(site)                                                # consultando o site e retornando o html para a variável 'page'
        soup = BeautifulSoup(page, 'html5lib')                                      # agora soup tem o html da página
        popular = (soup.find('h3', class_='featured-item-name')).text.strip()       # extrai o nome do artista
        popular = popular.replace('\"','')                                          # trarando o nome para colocar no banco de dados
    except Exception as e:
            print(e)
    return popular

def salvaEmXMLArtista(artistas):
    xml_file = open('music.xml', 'w+')
    root = etree.Element('Artistas')
    for artista in artistas:
        try:
            etree.SubElement(root, 'Artista', id=artista[0], nomeartistico=artista[0], genero=artista[1], cidade=artista[2], pais=artista[3], musicadasmena=artista[4])
        except Exception as e:
            print(e)
    tree = etree.ElementTree(root)
    tree.write('music.xml', encoding='utf-8', xml_declaration=True, pretty_print=True)

def salvaEmXMLFilme(filmes):
    xml_file = open('movie.xml', 'w+')
    root = etree.Element('Filmes')
    for filme in filmes:
        try:
            etree.SubElement(root, 'Filme', nome=filme[0], genero=filme[1], data=filme[2], diretor=filme[3], receita=filme[4])
        except Exception as e:
            print(e)
    tree = etree.ElementTree(root)
    tree.write('movie.xml', encoding='utf-8', xml_declaration=True, pretty_print=True)

def menu():
    banquetaDeCubos = BancoDeDados()
    banquetaDeCubos.conectarBancoDeDados()

    artistas = []
    filmes = []

    id_artistas = banquetaDeCubos.listarDeTabela('id', 'artista_musical')
    id_filmes = banquetaDeCubos.listarDeTabela('id', 'filme')

    # extraindo dados dos artistas musicais
    for i in range(1,len(id_artistas)):
        artista = []
        musica_mais_popular_da_semana = extraiDolastfm(id_artistas[i][0])
        nome, genero, cidade, pais = extraiDaWikiArtista(id_artistas[i][0])
        artista.append(nome)
        artista.append(",".join(genero))
        artista.append(cidade)
        artista.append(pais)
        artista.append(musica_mais_popular_da_semana)
        artistas.append(artista)
       
    # extraindo dados dos filmes
    for j in range(1,len(id_filmes)):
        filme = []
        nome, genero, data, diretor = extraiDoImdb(id_filmes[j][0])
        receita = extraiDaWikiFilme(nome)
        filme.append(nome)
        filme.append(genero)
        filme.append(data)
        filme.append(diretor)
        filme.append(receita)
        filmes.append(filme)
        print filme

    # salvando os arquivos obtidos em xml
    salvaEmXMLArtista(artistas)
    salvaEmXMLFilme(filmes)


def main():
    menu()

if __name__ == '__main__':
    main()